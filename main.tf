provider "google" {
  credentials = file("./terraform_service_account.json")
  project     = var.gcp_default_project
  region      = var.gcp_default_region
}

module "vpc_network" {
  source = "./compute-vpc"
   gcp_default_region = var.gcp_default_region
}

module "compute_engine" {
  source = "./compute-engine"
  network_name       = module.vpc_network.network_name
  subnetwork_name    = module.vpc_network.subnetwork_name
  gcp_default_region = var.gcp_default_region
  gcp_default_zone   = var.gcp_default_zone
}