variable "gcp_default_project" {
  type        = string
  default     = ""
  description = "Default project of the whole environment"
}

variable "gcp_default_region" {
  type        = string
  default     = "europe-west1"
  description = "Default region of the whole environment"
}

variable "gcp_default_zone" {
  type        = string
  default     = "europe-west1-b"
  description = "Default zone of the whole environment"
}
