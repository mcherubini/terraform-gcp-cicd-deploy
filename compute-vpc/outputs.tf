output "network_name" {
  value       = google_compute_network.vpc_network.name
  description = "The name of the VPC Network created"
}

output "subnetwork_name" {
  value       = google_compute_subnetwork.my-subnetwork.name
  description = "The name of the VPC SubNetwork created"
}